# JapaneseTranslationProgress

## English

Log of [Translation Progress for Japanese in crowdin](https://crowdin.com/project/gitlab-ee).
[The issue](https://gitlab.com/gitlab-org/gitlab/-/issues/216506)'s goal is 80% approval translation for Japanese at end of this year.

## Japanese

GitLabの[日本語翻訳の進捗](https://crowdin.com/project/gitlab-ee)の記録です。
[課題](https://gitlab.com/gitlab-org/gitlab/-/issues/216506) は 2020-12-31 に承認済み日本語翻訳を80%に上げることです。


# Progress data

|day       | Translation Rate(%) | Approval Rate(%) | Note|
|:--------:| :---------------:| :------------:| :---- |
|2020-05-05| 97 | 51 | [The issue](https://gitlab.com/gitlab-org/gitlab/-/issues/216506) day |
|2020-05-06| 96	| 55 | New 312 strings are added to gitlab, Japanese vacation season that is called "golden week" is over | 
|2020-05-07| 98	| 64 | Thanks for our translator's voting |
|2020-05-08| 98	| 65 |  |
|2020-05-09| 98	| 67 |  |
|2020-05-10| 98	| 68 |  |
|2020-05-11| 98	| 70 |  |
|2020-05-12| 98	| 71 |  |
|2020-05-13| 98	| 71 | GitLab Hackathon 2Q '2020 in Japanese |
|2020-05-14| 98	| 72 |  |
|2020-05-15| 98	| 73 |  |
|2020-05-16| 98	| 74 | Anounce the progress in #translation channel of gitlab-jp slack again |
|2020-05-17| 98	| 78 |  |
|2020-05-18| 98	| 79 |  |
|2020-05-19| 98	| 79 |  |
|2020-05-20| 98	| 80 | Done ! |
|2020-05-21| 98	| 80 |  |
|2020-05-22| 98	| 80 |  |
|2020-05-23| 98	| 80 |  |
|2020-05-24| 98	| 80 |  |
|2020-05-25| 98	| 80 |  |
|2020-05-26| 98	| 80 |  |
|2020-05-27| 98	| 80 |  |
|2020-05-28| 98	| 80 |  |
|2020-05-29| 98	| 80 |  |
|2020-05-30| 98	| 80 |  |

## Note

- day is YYYY-MM-DD format
- Golden Week is short holyday season in Japan, 4/29 - 5/6.
